package controllers

import (
	"fmt"
	"time"
	"math"
	"regexp"
	"strconv"
	"strings"
	"net/http"
	"io/ioutil"
	"encoding/json"

	"golang-twitter/models"
	"golang-twitter/connection"

	"github.com/labstack/echo/v4"
	"github.com/tmdvs/Go-Emoji-Utils"
)

const BaseURL = "http://localhost:1770"
const KeyWords = "%40danawallet"

// ===== ***** ===== SCRAPPING ===== ***** ===== //
func ConvertToUint (input string) uint64 {
	u64, err := strconv.ParseUint(input, 10, 64)
    if err != nil { fmt.Println(err) }
    wd := uint64(u64)
    return wd
}
// ======== DATA GRABBER ======== //
//GET DATA TWEET DAILY
func GetTweetData (c echo.Context) error {
	currentTime := time.Now()

	var responseData models.ResponseJSON
	var responseDataMax models.ResponseJSON
	var arrData []models.ReturnTweet
	var maxNumber uint64

	//FIRST
	responseFirst, err := http.Get(fmt.Sprintf("%s/tweet-grabber/%s", BaseURL, KeyWords))
	if err != nil { fmt.Println(err.Error()) }

	defer responseFirst.Body.Close()
	bodyFirst, err := ioutil.ReadAll(responseFirst.Body)
	if err != nil { fmt.Println(err.Error()) }

	err = json.Unmarshal(bodyFirst, &responseData)
	if err != nil { fmt.Println(err.Error()) }
	
	for _, dataLoop := range responseData.STATUSES {
		Loop := models.ReturnTweet {
			CREATED_AT : dataLoop.CREATED_AT,
			ID_STR : dataLoop.ID_STR,
			FULL_TEXT : dataLoop.FULL_TEXT,
			USER : dataLoop.USER.SCREEN_NAME,
		}

		arrData = append(arrData, Loop)
		maxNumber = ConvertToUint(dataLoop.ID_STR)
	}
	// END FIRST

	// SECOND
	responseMax, err := http.Get(fmt.Sprintf("%s/tweet-grabber-by-id/%s/%d", BaseURL, KeyWords, maxNumber))
	if err != nil { fmt.Println(err.Error()) }

	defer responseMax.Body.Close()
	bodyMax, err := ioutil.ReadAll(responseMax.Body)
	if err != nil { fmt.Println(err.Error()) }

	err = json.Unmarshal(bodyMax, &responseDataMax)
	if err != nil { fmt.Println(err.Error()) }

	for _, dataLoopMax := range responseDataMax.STATUSES {
		LoopMax := models.ReturnTweet {
			CREATED_AT : dataLoopMax.CREATED_AT,
			ID_STR : dataLoopMax.ID_STR,
			FULL_TEXT : dataLoopMax.FULL_TEXT,
			USER : dataLoopMax.USER.SCREEN_NAME,
		}

		arrData = append(arrData, LoopMax)
	}
	// END SECOND

	// SAVE JSON
	file, err := json.MarshalIndent(arrData, "", " ")
	if err != nil { fmt.Println(err.Error()) }
	
	fileName := fmt.Sprintf("twitter-%s.json", currentTime.Format("02-01-2006-150405"))

	err = ioutil.WriteFile(fmt.Sprintf("storages/raw/daily/%s", fileName), file, 0644)
	if err != nil { fmt.Println(err.Error()) }
	// END SAVE JSON

	// SAVE DB
	db := connection.DbConnMysql()
	defer db.Close()

	query := fmt.Sprintf("INSERT INTO raw_daily (rawday_name) VALUES ('%s')", fileName)
	_, errSave := db.Exec(query)
	if errSave != nil { fmt.Println(errSave.Error()) }
	// END SAVE

	return c.JSON(http.StatusOK, arrData)
}

// GET DATA TWEET WEEKLY
func GetTweetWeekly (c echo.Context) error {
	currentTime := time.Now()

	var responseData models.ResponseJSON
	var responseDataMax models.ResponseJSON
	var arrData []models.ReturnTweet
	var maxNumber uint64

	//FIRST
	responseFirst, err := http.Get(fmt.Sprintf("%s/tweet-grabber/%s", BaseURL, KeyWords))
	if err != nil { fmt.Println(err.Error()) }

	defer responseFirst.Body.Close()
	bodyFirst, err := ioutil.ReadAll(responseFirst.Body)
	if err != nil { fmt.Println(err.Error()) }

	err = json.Unmarshal(bodyFirst, &responseData)
	if err != nil { fmt.Println(err.Error()) }
	
	for _, dataLoop := range responseData.STATUSES {
		Loop := models.ReturnTweet {
			CREATED_AT : dataLoop.CREATED_AT,
			ID_STR : dataLoop.ID_STR,
			FULL_TEXT : dataLoop.FULL_TEXT,
			USER : dataLoop.USER.SCREEN_NAME,
		}

		arrData = append(arrData, Loop)
		maxNumber = ConvertToUint(dataLoop.ID_STR)
	}
	// END FIRST

	// LOOP
	loopNumber := 10
	for i := 0; loopNumber >= 10; i++ {
		// SECOND
		responseMax, err := http.Get(fmt.Sprintf("%s/tweet-grabber-by-id/%s/%d", BaseURL, KeyWords, maxNumber))
		if err != nil { fmt.Println(err.Error()) }

		defer responseMax.Body.Close()
		bodyMax, err := ioutil.ReadAll(responseMax.Body)
		if err != nil { fmt.Println(err.Error()) }

		err = json.Unmarshal(bodyMax, &responseDataMax)
		if err != nil { fmt.Println(err.Error()) }

		for Mi, dataLoopMax := range responseDataMax.STATUSES {
			LoopMax := models.ReturnTweet {
				CREATED_AT : dataLoopMax.CREATED_AT,
				ID_STR : dataLoopMax.ID_STR,
				FULL_TEXT : dataLoopMax.FULL_TEXT,
				USER : dataLoopMax.USER.SCREEN_NAME,
			}

			arrData = append(arrData, LoopMax)
			maxNumber = ConvertToUint(dataLoopMax.ID_STR)
			loopNumber = Mi
		}
		// END SECOND
	}   
	// END LOOP

	// SAVE JSON
	file, err := json.MarshalIndent(arrData, "", " ")
	if err != nil { fmt.Println(err.Error()) }
	
	fileName := fmt.Sprintf("twitter-weekly-%s.json", currentTime.Format("02-01-2006-150405"))

	err = ioutil.WriteFile(fmt.Sprintf("storages/raw/weekly/%s", fileName), file, 0644)
	if err != nil { fmt.Println(err.Error()) }
	// END SAVE JSON

	// SAVE DB
	db := connection.DbConnMysql()
	defer db.Close()

	query := fmt.Sprintf("INSERT INTO raw_weekly (rawweek_name) VALUES ('%s')", fileName)
	_, errSave := db.Exec(query)
	if errSave != nil { fmt.Println(errSave.Error()) }
	// END SAVE

	return c.JSON(http.StatusOK, arrData)
}

// GET TWEET DATA BY ID LIMIT 1
func TweetById (c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil { fmt.Println(err.Error()) }

	var responseData models.SearchTweet

	response, err := http.Get(fmt.Sprintf("%s/tweet-grabber-by-limit/%d", BaseURL, id))
	if err != nil { fmt.Println(err.Error()) }

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil { fmt.Println(err.Error()) }

	err = json.Unmarshal(body, &responseData)
	if err != nil { fmt.Println(err.Error()) }

	returnData := models.ReturnTweet {
		CREATED_AT : responseData.DATA.CREATED_AT,
		ID_STR : responseData.DATA.ID,
		FULL_TEXT : responseData.DATA.TEXT,
		USER : responseData.INCLUDES.USERS[0].USERNAME,
	}

	return c.JSON(http.StatusOK, returnData)
}
// ======== END DATA GRABBER ======== //

// ======== DATA MANIPULATE ======== //
// MERGE DATA FROM LOCAL
func MergeData (c echo.Context) error {
	DataType := c.Param("type")
	var ResultData []models.ReturnTweet

	errorResponse := models.ErrorResponse {
		ERROR : true,
		MESSAGE : "DATA NOT FOUND",
	}

	db := connection.DbConnMysql()

	if DataType == "daily" {
		sqlStatement := "SELECT * FROM raw_daily"
		
		rows, err := db.Query(sqlStatement)
		if err != nil { return c.JSON(http.StatusOK, errorResponse) }
		defer db.Close()
		
		for rows.Next() {
			var data models.DataDaily
			
			err = rows.Scan(&data.RAWDAY_ID, &data.RAWDAY_NAME, &data.RAWDAY_CREATED)
			if err != nil { fmt.Println(err.Error()) }

			ResultData = append(ResultData, ReadData("daily", data.RAWDAY_NAME)...)
		}

		// Clean JSON Data
		cleanJson := RemoveDuplicate(ResultData)

		return c.JSON(http.StatusOK, cleanJson)

	} else if DataType == "weekly" {
		sqlStatement := "SELECT * FROM raw_weekly"
		
		rows, err := db.Query(sqlStatement)
		if err != nil { return c.JSON(http.StatusOK, errorResponse) }
		defer db.Close()
		
		for rows.Next() {
			var dataWeek models.DataWeekly
			
			err = rows.Scan(&dataWeek.RAWWEEK_ID, &dataWeek.RAWWEEK_NAME, &dataWeek.RAWWEEK_CREATED)
			if err != nil { fmt.Println(err.Error()) }

			ResultData = append(ResultData, ReadData("weekly", dataWeek.RAWWEEK_NAME)...)
		}

		// Clean JSON Data
		cleanJson := RemoveDuplicate(ResultData)

		return c.JSON(http.StatusOK, cleanJson)

	} else if DataType == "repaired" {
		sqlStatement := "SELECT * FROM raw_repaired"
		
		rows, err := db.Query(sqlStatement)
		if err != nil { return c.JSON(http.StatusOK, errorResponse) }
		defer db.Close()
		
		for rows.Next() {
			var dataRepair models.DataRepaired
			
			err = rows.Scan(&dataRepair.RAWREPAIR_ID, &dataRepair.RAWREPAIR_NAME, &dataRepair.RAWREPAIR_CREATED)
			if err != nil { fmt.Println(err.Error()) }

			ResultData = append(ResultData, ReadData("repaired", dataRepair.RAWREPAIR_NAME)...)
		}

		// Clean JSON Data
		cleanJson := RemoveDuplicate(ResultData)

		return c.JSON(http.StatusOK, cleanJson)
	}

	return c.JSON(http.StatusOK, errorResponse)
}

// MAKE DATA TEST
func MakeData (c echo.Context) error {
	currentTime := time.Now()
	db := connection.DbConnMysql()
	defer db.Close()

	var ResultData []models.ReturnTweet
	errorResponse := models.ErrorResponse {
		ERROR : true,
		MESSAGE : "DATA NOT FOUND",
	}

	// GET DATA FROM RAWDAILY TABLE
	sqlStatementDaily := "SELECT * FROM raw_daily"
	rowsDaily, err := db.Query(sqlStatementDaily)
	if err != nil { return c.JSON(http.StatusOK, errorResponse) }
	
	for rowsDaily.Next() {
		var DataDaily models.DataDaily
		
		err = rowsDaily.Scan(&DataDaily.RAWDAY_ID, &DataDaily.RAWDAY_NAME, &DataDaily.RAWDAY_CREATED)
		if err != nil { fmt.Println(err.Error()) }

		// MERGE ARRAY
		ResultData = append(ResultData, ReadData("daily", DataDaily.RAWDAY_NAME)...)
	}
	ResultData = RemoveDuplicate(ResultData)

	// GET DATA FROM RAWWEEKLY TABLE
	sqlStatementWeekly := "SELECT * FROM raw_weekly"
	rowsWeekly, err := db.Query(sqlStatementWeekly)
	if err != nil { return c.JSON(http.StatusOK, errorResponse) }
	
	for rowsWeekly.Next() {
		var DataWeekly models.DataWeekly
		
		err = rowsWeekly.Scan(&DataWeekly.RAWWEEK_ID, &DataWeekly.RAWWEEK_NAME, &DataWeekly.RAWWEEK_CREATED)
		if err != nil { fmt.Println(err.Error()) }

		// MERGE ARRAY
		ResultData = append(ResultData, ReadData("weekly", DataWeekly.RAWWEEK_NAME)...)
	}
	ResultData = RemoveDuplicate(ResultData)

	// GET DATA FROM RAWREPAIRED TABLE
	sqlStatementRepaired := "SELECT * FROM raw_repaired"
	rowsRepaired, err := db.Query(sqlStatementRepaired)
	if err != nil { return c.JSON(http.StatusOK, errorResponse) }
	
	for rowsRepaired.Next() {
		var DataRepaired models.DataRepaired
		
		err = rowsRepaired.Scan(&DataRepaired.RAWREPAIR_ID, &DataRepaired.RAWREPAIR_NAME, &DataRepaired.RAWREPAIR_CREATED)
		if err != nil { fmt.Println(err.Error()) }

		// MERGE ARRAY
		ResultData = append(ResultData, ReadData("repaired", DataRepaired.RAWREPAIR_NAME)...)
	}
	ResultData = RemoveDuplicate(ResultData)

	// Clean JSON Data
	cleanJson := RemoveDuplicate(ResultData)

	//DATA RAW TESTING
	var DataRaw []models.DataRaw
	for _, rd := range cleanJson {
		DataRaw = append(DataRaw, models.DataRaw {
			CREATED_AT : rd.CREATED_AT,
			ID_STR : rd.ID_STR,
			FULL_TEXT : rd.FULL_TEXT,
			USER : rd.USER,
			LABEL : "",
		})
	}

	// SAVE JSON DB
	file, err := json.MarshalIndent(DataRaw, "", " ")
	if err != nil { fmt.Println(err.Error()) }
	
	fileName := fmt.Sprintf("testing-%s.json", currentTime.Format("02-01-2006-150405"))

	err = ioutil.WriteFile(fmt.Sprintf("storages/data/testing/%s", fileName), file, 0644)
	if err != nil { fmt.Println(err.Error()) }

	query := fmt.Sprintf("INSERT INTO testing (test_name) VALUES ('%s')", fileName)
	_, errSave := db.Exec(query)
	if errSave != nil { fmt.Println(errSave.Error()) }

	return c.JSON(http.StatusOK, DataRaw)
}

// GET TEST DATA BY ID
func GetTest (c echo.Context) error {
	inputID := c.Param("id")
	var testName string

	db := connection.DbConnMysql()
	defer db.Close()

	// GET DATA BY ID
	sqlStatement := fmt.Sprintf("SELECT test_name FROM testing WHERE test_id = '%s'", inputID)
	row := db.QueryRow(sqlStatement)
	errRow := row.Scan(&testName)
	if errRow != nil { fmt.Println(errRow.Error()) }

	// READ DATA
	var DataTwitter []models.ReturnTweet
	DataFile := fmt.Sprintf("storages/data/testing/%s", testName)
	
	file, err1 := ioutil.ReadFile(DataFile)
	if err1 != nil { fmt.Printf( "// error while reading file %s\n", DataFile ) }

	errRead := json.Unmarshal(file, &DataTwitter)
	if errRead != nil { fmt.Println(errRead.Error()) }

	return c.JSON(http.StatusOK, DataTwitter)
}

// DEV
// REPAIR DATA (DATA LAMA TIDAK BISA DI)
func RepairData (c echo.Context) error {
	var DataTwitter []models.OldTweet
	var arrString []string

	var responseData models.SearchTweet
	var arrResponse []models.ReturnTweet
	
	files, err := ioutil.ReadDir("./storages/repair")
    if err != nil { fmt.Println(err.Error()) }

	// LOOP MERGE FILES 
    for _, f := range files {
		arrString = append(arrString, f.Name())

		DataFile := fmt.Sprintf("storages/repair/%s", f.Name())

		file, err1 := ioutil.ReadFile( DataFile )
		if err1 != nil { fmt.Printf( "// error while reading file %s\n", DataFile ) }

		var DataLoop []models.OldTweet

		err := json.Unmarshal(file, &DataLoop)
		if err != nil { fmt.Println(err.Error()) }

		DataTwitter = append(DataTwitter, DataLoop...)
	}

	// REPAIR DATA
	for _, dt := range DataTwitter {
		response, err := http.Get(fmt.Sprintf("%s/tweet-grabber-by-limit/%d", BaseURL, dt.ID))
		if err != nil { fmt.Println(err.Error()) }

		defer response.Body.Close()
		body, err := ioutil.ReadAll(response.Body)
		if err != nil { fmt.Println(err.Error()) }

		err = json.Unmarshal(body, &responseData)
		if err != nil { fmt.Println(err.Error()) }

		LoopResponse := models.ReturnTweet {
			CREATED_AT : responseData.DATA.CREATED_AT,
			ID_STR : responseData.DATA.ID,
			FULL_TEXT : responseData.DATA.TEXT,
			USER : responseData.INCLUDES.USERS[0].USERNAME,
		}

		arrResponse = append(arrResponse, LoopResponse)
	}

	// SAVE JSON
	currentTime := time.Now()
	file, err := json.MarshalIndent(arrResponse, "", " ")
	if err != nil { fmt.Println(err.Error()) }
	
	fileName := fmt.Sprintf("twitter-repair-%s.json", currentTime.Format("02-01-2006-150405"))

	err = ioutil.WriteFile(fmt.Sprintf("storages/raw/repaired/%s", fileName), file, 0644)
	if err != nil { fmt.Println(err.Error()) }
	// END SAVE JSON

	// SAVE DB
	db := connection.DbConnMysql()
	defer db.Close()

	query := fmt.Sprintf("INSERT INTO raw_repaired (rawrepair_name) VALUES ('%s')", fileName)
	_, errSave := db.Exec(query)
	if errSave != nil { fmt.Println(errSave.Error()) }
	// END SAVE

	return c.JSON(http.StatusOK, arrResponse)
}
// END DEV

// ----- HELPER -----

// READ JSON FILE
func ReadData (jsonSource string, jsonFile string) []models.ReturnTweet {
	var DataTwitter []models.ReturnTweet
	DataFile := fmt.Sprintf("storages/raw/%s/%s", jsonSource, jsonFile)
	
	file, err1 := ioutil.ReadFile( DataFile )
	if err1 != nil { fmt.Printf( "// error while reading file %s\n", DataFile ) }

	err := json.Unmarshal(file, &DataTwitter)
	if err != nil { fmt.Println(err.Error()) }

	return DataTwitter
}

// REMOVE DUPLICATE ->> MERGE DATA
func RemoveDuplicate (dataSource []models.ReturnTweet) []models.ReturnTweet {
	var uniqueData []models.ReturnTweet
	for _, rd := range dataSource {
		skip := false

		// REMOVE TWEET FROM DANAWALLET
		if (rd.USER == "danawallet") {
			skip = true
		} else {
			for _, un := range uniqueData {
				if (rd == un) || (rd.FULL_TEXT == un.FULL_TEXT) || (rd.ID_STR == un.ID_STR) {
					skip = true
					break
				}
			}
		}

		if !skip {
			uniqueData = append(uniqueData, models.ReturnTweet {
				CREATED_AT : rd.CREATED_AT,
				ID_STR : rd.ID_STR,
				FULL_TEXT : rd.FULL_TEXT,
				USER : rd.USER,
			})
		}
	}

	return uniqueData
}
// ======== END DATA MANIPULATE ======== //
// ===== ***** ===== END SCRAPPING ===== ***** ===== //

// ===== ***** ===== TEXT PREPROCESSING ===== ***** ===== //
// DATA TRAINING
// TEXT PREPROCESSING
func TextPrepo (c echo.Context) error {
	inputID := c.Param("id")
	inputType := c.Param("type")

	var testName string
	var sqlStatement string

	db := connection.DbConnMysql()
	defer db.Close()

	// GET DATA BY ID
	if (inputType == "training") {
		sqlStatement = fmt.Sprintf("SELECT train_name FROM training WHERE train_id = '%s'", inputID)
	} else if (inputType == "testing") {
		sqlStatement = fmt.Sprintf("SELECT test_name FROM testing WHERE test_id = '%s'", inputID)
	}

	row := db.QueryRow(sqlStatement)
	errRow := row.Scan(&testName)
	if errRow != nil { fmt.Println(errRow.Error()) }

	// READ DATA
	var DataTwitter []models.DataRaw
	DataFile := fmt.Sprintf("storages/data/%s/%s", inputType, testName)
	
	file, err1 := ioutil.ReadFile(DataFile)
	if err1 != nil { fmt.Printf( "// error while reading file %s\n", DataFile ) }

	errRead := json.Unmarshal(file, &DataTwitter)
	if errRead != nil { fmt.Println(errRead.Error()) }

	//TOKENIZE TEXT
	tokenizeData := TokenizeText(DataTwitter, inputType)

	return c.JSON(http.StatusOK, tokenizeData)
}

// COUNT TF IDF
func TermFreqInvDocFreq (c echo.Context) error {
	inputID := c.Param("id")
	inputType := c.Param("type")
	currentTime := time.Now()

	db := connection.DbConnMysql()
	defer db.Close()

	var steamName string

	// GET DATA BY ID
	sqlStatement := fmt.Sprintf("SELECT steam_name FROM steamming WHERE steam_id = '%s' AND steam_status = '%s'", inputID, inputType)
	row := db.QueryRow(sqlStatement)
	errRow := row.Scan(&steamName)
	if errRow != nil { fmt.Println(errRow.Error()) }

	// READ DATA
	var DataSteam []models.TokenWord
	DataFile := fmt.Sprintf("storages/data/steamming/%s", steamName)
	
	file, err1 := ioutil.ReadFile(DataFile)
	if err1 != nil { fmt.Printf( "// error while reading file %s\n", DataFile ) }

	errRead := json.Unmarshal(file, &DataSteam)
	if errRead != nil { fmt.Println(errRead.Error()) }

	// BAG OF WORDS
	var allText []string

	for _, dt := range DataSteam {
		for _, ardt := range dt.TEXT {
			skip := false
			// check duplicate word
			for _, alt := range allText {
				if (alt == ardt) {
					skip = true
					break
				}
			}
			if !skip {
				allText = append(allText, ardt)
			}
		}
	}

	// TERM FREQUENCY
	var termFreqData []models.TF

	for _, dst := range DataSteam {
		var tfArr []int

		// looop bag of word
		for _, bow := range allText {
			countText := 0
			//loop text
			for _, dtt := range dst.TEXT {
				if (bow == dtt) {
					countText = countText + 1
				}
			}

			tfArr = append(tfArr, countText)
		}

		termFreqData = append(termFreqData, models.TF {
			ID : dst.ID,
			LABEL : dst.LABEL,
			BAG : allText,
			TEXT : strings.Join(dst.TEXT[:]," "),
			TF : tfArr,
		})
	}

	// SAVE TERM JSON
	file, err := json.MarshalIndent(termFreqData, "", " ")
	if err != nil { fmt.Println(err.Error()) }
	
	fileName := fmt.Sprintf("term-freq-%s.json", currentTime.Format("02-01-2006-150405"))

	err = ioutil.WriteFile(fmt.Sprintf("storages/data/term/tf/%s", fileName), file, 0644)
	if err != nil { fmt.Println(err.Error()) }
	// END SAVE TERM JSON
	// END TERM FREQUENCY

	// DOCUMENT FREQUENCY
	var nlgth []int
	for i := 0; i < len(allText); i++ {
		nlgth = append(nlgth, 0)
	}

	// loop doc
	for _, tf := range termFreqData {
		// loop tf
		for i, tff := range tf.TF {
			if (tff > 0) {
				nlgth[i] = nlgth[i] + 1
			} else {
				nlgth[i] = nlgth[i] + 0
			}
		}
	}

	fileNameDoc := fmt.Sprintf("docs-freq-%s.json", currentTime.Format("02-01-2006-150405"))
	docFreqData := models.DF {
		ID : fileNameDoc,
		BAG : allText,
		DF : nlgth,
	}

	// SAVE TERM JSON
	fileDoc, err := json.MarshalIndent(docFreqData, "", " ")
	if err != nil { fmt.Println(err.Error()) }

	err = ioutil.WriteFile(fmt.Sprintf("storages/data/term/df/%s", fileNameDoc), fileDoc, 0644)
	if err != nil { fmt.Println(err.Error()) }
	// END SAVE TERM JSON
	// END DOCUMENT FREQUENCY

	// INVERSE DOCUMENT FREQUENCY
	var arrIDF []float64
	countDoc := len(termFreqData)
	for _, dfd := range docFreqData.DF {
		arrIDF = append(arrIDF, math.Log(float64(countDoc)/float64(dfd)) + float64(1))
	}

	fileNameInv := fmt.Sprintf("inverse-docs-freq-%s.json", currentTime.Format("02-01-2006-150405"))
	inverseDocFreqData := models.IDF {
		ID : fileNameDoc,
		BAG : allText,
		DF : nlgth,
		IDF : arrIDF,
	}

	// SAVE IDF JSON
	fileInv, err := json.MarshalIndent(inverseDocFreqData, "", " ")
	if err != nil { fmt.Println(err.Error()) }

	err = ioutil.WriteFile(fmt.Sprintf("storages/data/term/idf/%s", fileNameInv), fileInv, 0644)
	if err != nil { fmt.Println(err.Error()) }
	// END SAVE IDF JSON
	// END INVERSE DOCUMENT FREQUENCY

	// TF IDF
	var TFIDFData []models.TFIDF
	for _, ttff := range termFreqData {
		var arrTFIDF []float64
		for i, ltff := range ttff.TF {
			if (ltff > 0) {
				arrTFIDF = append(arrTFIDF, (arrIDF[i] * float64(ltff)))
			} else {
				arrTFIDF = append(arrTFIDF, float64(0))
			}
		}

		tfidfData := models.TFIDF {
			ID : ttff.ID,
			LABEL : ttff.LABEL,
			BAG : ttff.BAG,
			TEXT : ttff.TEXT,
			TF : ttff.TF,
			TFIDF : arrTFIDF,
		}

		TFIDFData = append(TFIDFData, tfidfData)
	}

	fileNameTFIDF := fmt.Sprintf("tf-idf-%s.json", currentTime.Format("02-01-2006-150405"))

	// SAVE TFIDF JSON
	fileTFIDF, err := json.MarshalIndent(TFIDFData, "", " ")
	if err != nil { fmt.Println(err.Error()) }

	err = ioutil.WriteFile(fmt.Sprintf("storages/data/term/tfidf/%s", fileNameTFIDF), fileTFIDF, 0644)
	if err != nil { fmt.Println(err.Error()) }
	// END SAVE TFIDF JSON
	// END TFIDF

	// FINAL TFIDF
	fileNameFinalTFIDF := fmt.Sprintf("final-tfidf-%s.json", currentTime.Format("02-01-2006-150405"))
	finalTFIDF := models.FinalTFIDF {
		ID : fileNameFinalTFIDF,
		DF : nlgth,
		IDF : arrIDF,
		TFIDF : TFIDFData,
	}


	// SAVE FINAL TFIDF JSON
	fileFinalTFIDF, err := json.MarshalIndent(finalTFIDF, "", " ")
	if err != nil { fmt.Println(err.Error()) }

	err = ioutil.WriteFile(fmt.Sprintf("storages/data/term/final/%s", fileNameFinalTFIDF), fileFinalTFIDF, 0644)
	if err != nil { fmt.Println(err.Error()) }
	// END SAVE FINAL TFIDF JSON
	//END FINAL TFIDF

	//SAVE DATABASE
	query := fmt.Sprintf("INSERT INTO term (term_source, term_tf, term_df, term_idf, term_tfidf, term_tfidf_final) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", steamName, fileName, fileNameDoc, fileNameInv, fileNameTFIDF, fileNameFinalTFIDF)
	_, errSave := db.Exec(query)
	if errSave != nil { fmt.Println(errSave.Error()) }

	return c.JSON(http.StatusOK, finalTFIDF)
}

// ADD STOPWORDS (SINGLE)
func AddStopwords (c echo.Context) error {
	currentTime := time.Now()
	// READ STOPWORDS FILE
	var dataStopwords []models.StopWords

	dataList := "storages/list/stopword.json"
	fileRead, err1 := ioutil.ReadFile(dataList)
	if err1 != nil { fmt.Printf( "// error while reading file %s\n", dataList) }

	errRead := json.Unmarshal(fileRead, &dataStopwords)
	if errRead != nil { fmt.Println(errRead.Error()) }

	if (c.FormValue("stopwords") != "") {
		newStopword := models.StopWords {
			ID : currentTime.Format("02012006150405"),
			TEXT : c.FormValue("stopwords"),
		}
	
		dataStopwords = append(dataStopwords, newStopword)
	}

	// SAVE STOPWORDS FILE
	fileSave, err := json.MarshalIndent(dataStopwords, "", " ")
	if err != nil { fmt.Println(err.Error()) }

	err = ioutil.WriteFile("storages/list/stopword.json", fileSave, 0644)
	if err != nil { fmt.Println(err.Error()) }

	return c.JSON(http.StatusOK, dataStopwords)
}

// ADD STOPWORDS (ARRAY)
func AddStopwordsArray (c echo.Context) error {
	currentTime := time.Now()

	var textSource models.ArrayStopwords
	_ = c.Bind(&textSource)

	// READ STOPWORDS FILE
	var dataStopwords []models.StopWords

	dataList := "storages/list/stopword.json"
	fileRead, err1 := ioutil.ReadFile(dataList)
	if err1 != nil { fmt.Printf( "// error while reading file %s\n", dataList) }

	errRead := json.Unmarshal(fileRead, &dataStopwords)
	if errRead != nil { fmt.Println(errRead.Error()) }

	// loop array data
	for _, txt := range textSource.TEXT {
		newStopword := models.StopWords {
			ID : currentTime.Format("02012006150405"),
			TEXT : txt,
		}
	
		dataStopwords = append(dataStopwords, newStopword)
	}

	// SAVE STOPWORDS FILE
	fileSave, err := json.MarshalIndent(dataStopwords, "", " ")
	if err != nil { fmt.Println(err.Error()) }

	err = ioutil.WriteFile("storages/list/stopword.json", fileSave, 0644)
	if err != nil { fmt.Println(err.Error()) }

	return c.JSON(http.StatusOK, dataStopwords)
}

// HELPER
// TOKENIZE TEXT (lowercase, clear emoji unused symbols, separated words, stopwords)
func TokenizeText (textSource []models.DataRaw, statusType string) []models.TokenWord {
	var tokenizeData []models.TokenWord

	db := connection.DbConnMysql()
	defer db.Close()

	for _, dt := range textSource {
		loop := models.TokenWord {
			ID : dt.ID_STR,
			LABEL : dt.LABEL,
			TEXT : strings.Fields(SteamingText(ClearStopwords(strings.ToLower(ClearMention(RemoveEmoji(dt.FULL_TEXT)))))),
		}

		tokenizeData = append(tokenizeData, loop)
	}

	// SAVE STEAM DB
	currentTime := time.Now()
	file, err := json.MarshalIndent(tokenizeData, "", " ")
	if err != nil { fmt.Println(err.Error()) }
	
	fileName := fmt.Sprintf("steamming-%s.json", currentTime.Format("02-01-2006-150405"))

	err = ioutil.WriteFile(fmt.Sprintf("storages/data/steamming/%s", fileName), file, 0644)
	if err != nil { fmt.Println(err.Error()) }

	query := fmt.Sprintf("INSERT INTO steamming (steam_name, steam_status) VALUES ('%s', '%s')", fileName, statusType)
	_, errSave := db.Exec(query)
	if errSave != nil { fmt.Println(errSave.Error()) }

	return tokenizeData

	// STEP
	// 1. Remove emoji
	// 2. Clear mentions
	// 3. Lower text
	// 4. Word Fix ---->> belum
	// 5. Clear Stopwords
	// 6. Stemming
}

// REMOVE EMOJI
func RemoveEmoji (textSource string) string {
	var quotation = strings.NewReplacer("\n", " ")
	result := emoji.RemoveAll(quotation.Replace(textSource))

	return result
}

// REMOVE MENTION + HASHTAG + LINK + SYMBOLS
func ClearMention (textSource string) string {
	var arrString []string
	sliceSC := strings.Fields(textSource)

	for _, ssc := range sliceSC {
		if (strings.Contains(ssc, "@") == true) || (strings.Contains(ssc, "#") == true) || (strings.Contains(ssc, "https:/") == true) || (strings.Contains(ssc, "https:") == true) || (strings.Contains(ssc, "https") == true) {
			continue;
		} else {
			arrString = append(arrString, ssc)
		}
	}

	result := strings.Join(arrString[:]," ")

	reg, err := regexp.Compile("[^a-zA-Z0-9]+")
    if err != nil { fmt.Println(err.Error()) }

	result = reg.ReplaceAllString(result, " ")

	return result
}

// STOPWORDS REMOVAL
func ClearStopwords (textSource string) string {
	var newText []string
	arrSource := strings.Fields(textSource)

	// READ STOPWORDS FILE
	var dataStopwords []models.StopWords

	dataList := "storages/list/stopword.json"
	fileRead, err1 := ioutil.ReadFile(dataList)
	if err1 != nil { fmt.Printf( "// error while reading file %s\n", dataList) }

	errRead := json.Unmarshal(fileRead, &dataStopwords)
	if errRead != nil { fmt.Println(errRead.Error()) }

	for _, asc := range arrSource {
		skip := false

		for _, dts := range dataStopwords {
			if (asc == dts.TEXT) {
				skip = true
				break
			}
		}

		if !skip {
			newText = append(newText, asc)
		}
	}

	return strings.Join(newText[:]," ")
}

// STEMMING
func SteamingText (textSource string) string {
	var newText []string

	arrSource := strings.Fields(textSource)

	for _, asc := range arrSource {
		skip := false

		// check kata baku
		if (SearchBaku(asc) == "SUCCESS") {
			skip = true
			newText = append(newText, asc)
		}

		// if kata != kata baku { penghapusan imbuhan }
		if !skip {
			infPart := strings.Split(asc, "")

			if (len(infPart) >= 5) {
				// check kata baku
				if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
					newText = append(newText, strings.Join(infPart[:],""))
					continue
				}
				// Inflectional Particle + Inflectional Possesive Pronoun
				infPartSuffix2 := strings.Join(infPart[len(infPart)-2:][:], "") // if 2 char suffix
				infPartSuffix3 := strings.Join(infPart[len(infPart)-3:][:], "") // if 3 char suffix

				// if 3 char - step 1
				if (infPartSuffix3 == "kah") || (infPartSuffix3 == "lah") || (infPartSuffix3 == "pun") || (infPartSuffix3 == "nya") {
					infPart = infPart[:len(infPart)-3]
				// if 2 char - step 2
				} else if (infPartSuffix2 == "ku") || (infPartSuffix2 == "mu") {
					infPart = infPart[:len(infPart)-2]
				}
				infPartNew := strings.Join(infPart[:],"")
				// END Inflectional Particle + Inflectional Possesive Pronoun

				// check kata baku
				if (SearchBaku(infPartNew) == "SUCCESS") {
					newText = append(newText, infPartNew)
					continue
				}

				// First Order Derivational Prefix - step 3
				firstDerPreffix4 := strings.Join(infPart[:len(infPart)-(len(infPart)-4)][:], "") // if 4 char prefix
				firstDerPreffix3 := strings.Join(infPart[:len(infPart)-(len(infPart)-3)][:], "") // if 3 char prefix
				firstDerPreffix2 := strings.Join(infPart[:len(infPart)-(len(infPart)-2)][:], "") // if 2 char prefix

				// if found -> step 4.b
				if (firstDerPreffix4 == "meng") || (firstDerPreffix4 == "meny") || (firstDerPreffix4 == "peng") || (firstDerPreffix4 == "peny") {
					// check kata baku
					if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
						newText = append(newText, strings.Join(infPart[:],""))
						continue
					}
					infPart = infPart[len(infPart)-(len(infPart)-4):]

					// Add Replacement
					if (firstDerPreffix4 == "meny" || firstDerPreffix4 == "peny") {
						// add S to prefix MENY || PENY
						infPart = append([]string{"s"}, infPart...)
					} else {
						infPart = infPart[:]
					}
					
					// check kata baku
					if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
						newText = append(newText, strings.Join(infPart[:],""))
						continue
					}
					// step 4.b
					suffix4B3 := strings.Join(infPart[len(infPart)-3:][:], "") //4.b suffix 3
					suffix4B2 := strings.Join(infPart[len(infPart)-2:][:], "") //4.b suffix 2
					suffix4B1 := strings.Join(infPart[len(infPart)-1:][:], "") //4.b suffix 1

					prefix4B4 := strings.Join(infPart[:len(infPart)-(len(infPart)-4)][:], "")
					prefix4B3 := strings.Join(infPart[:len(infPart)-(len(infPart)-3)][:], "")
					prefix4B2 := strings.Join(infPart[:len(infPart)-(len(infPart)-2)][:], "")

					if (suffix4B3 == "kan") {
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (ke, peng)
							if (prefix4B4 == "peng") || (prefix4B2 == "ke") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-3]
							}
						}
					} else if (suffix4B2 == "an") {
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (di, meng, ter)
							if (prefix4B4 == "meng") || (prefix4B3 == "ter") || (prefix4B2 == "di") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-2]
							}
						}
					} else if (suffix4B1 == "i") {
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (ke, peng, ber)
							if (prefix4B4 == "peng") || (prefix4B3 == "ber") || (prefix4B2 == "ke") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-1]
							}
						}
					} else {
						infPart = infPart[:]
					}

					// check kata baku
					if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
						newText = append(newText, strings.Join(infPart[:],""))
						continue
					}

					// step 5B- hapus Second Order Derivational Prefix
					secOrder5BPreffix3 := strings.Join(infPart[:len(infPart)-(len(infPart)-3)][:], "") // if 3 char prefix
					secOrder5BPreffix2 := strings.Join(infPart[:len(infPart)-(len(infPart)-2)][:], "") // if 3 char prefix

					if (secOrder5BPreffix3 == "ber") || (secOrder5BPreffix3 == "bel") || (secOrder5BPreffix3 == "per") || (secOrder5BPreffix3 == "pel") {
						// hapus preffix
						infPart = infPart[len(infPart)-(len(infPart)-3):]
					} else if (secOrder5BPreffix2 == "be") || (secOrder5BPreffix2 == "pe") {
						// hapus preffix
						infPart = infPart[len(infPart)-(len(infPart)-2):]
					}
				} else if (firstDerPreffix3 == "men") || (firstDerPreffix3 == "mem") || (firstDerPreffix3 == "pen") || (firstDerPreffix3 == "pem") || (firstDerPreffix3 == "ter") {
					// check kata baku
					if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
						newText = append(newText, strings.Join(infPart[:],""))
						continue
					}
					infPart = infPart[len(infPart)-(len(infPart)-3):]

					// check kata baku
					if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
						newText = append(newText, strings.Join(infPart[:],""))
						continue
					}
					// step 4.b
					suffix4B3 := strings.Join(infPart[len(infPart)-3:][:], "") //5.a suffix 3
					suffix4B2 := strings.Join(infPart[len(infPart)-2:][:], "") //5.a suffix 2
					suffix4B1 := strings.Join(infPart[len(infPart)-1:][:], "") //5.a suffix 1

					prefix4B4 := strings.Join(infPart[:len(infPart)-(len(infPart)-4)][:], "")
					prefix4B3 := strings.Join(infPart[:len(infPart)-(len(infPart)-3)][:], "")
					prefix4B2 := strings.Join(infPart[:len(infPart)-(len(infPart)-2)][:], "")

					if (suffix4B3 == "kan") {
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (ke, peng)
							if (prefix4B4 == "peng") || (prefix4B2 == "ke") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-3]
							}
						}
					} else if (suffix4B2 == "an") {
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (di, meng, ter)
							if (prefix4B4 == "meng") || (prefix4B3 == "ter") || (prefix4B2 == "di") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-2]
							}
						}
					} else if (suffix4B1 == "i") {
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (ke, peng, ber)
							if (prefix4B4 == "peng") || (prefix4B3 == "ber") || (prefix4B2 == "ke") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-1]
							}
						}
					} else {
						infPart = infPart[:]
					}

					// check kata baku
					if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
						newText = append(newText, strings.Join(infPart[:],""))
						continue
					}

					// step 5B- hapus Second Order Derivational Prefix
					secOrder5BPreffix3 := strings.Join(infPart[:len(infPart)-(len(infPart)-3)][:], "") // if 3 char prefix
					secOrder5BPreffix2 := strings.Join(infPart[:len(infPart)-(len(infPart)-2)][:], "") // if 3 char prefix

					if (secOrder5BPreffix3 == "ber") || (secOrder5BPreffix3 == "bel") || (secOrder5BPreffix3 == "per") || (secOrder5BPreffix3 == "pel") {
						// hapus preffix
						infPart = infPart[len(infPart)-(len(infPart)-3):]
					} else if (secOrder5BPreffix2 == "be") || (secOrder5BPreffix2 == "pe") {
						// hapus preffix
						infPart = infPart[len(infPart)-(len(infPart)-2):]
					}
				} else if (firstDerPreffix2 == "me") || (firstDerPreffix2 == "di") || (firstDerPreffix2 == "ke") {
					// check kata baku
					if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
						newText = append(newText, strings.Join(infPart[:],""))
						continue
					}
					infPart = infPart[len(infPart)-(len(infPart)-2):]

					// check kata baku
					if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
						newText = append(newText, strings.Join(infPart[:],""))
						continue
					}

					// step 4.b
					suffix4B3 := strings.Join(infPart[len(infPart)-3:][:], "") //5.a suffix 3
					suffix4B2 := strings.Join(infPart[len(infPart)-2:][:], "") //5.a suffix 2
					suffix4B1 := strings.Join(infPart[len(infPart)-1:][:], "") //5.a suffix 1

					prefix4B4 := strings.Join(infPart[:len(infPart)-(len(infPart)-4)][:], "")
					prefix4B3 := strings.Join(infPart[:len(infPart)-(len(infPart)-3)][:], "")
					prefix4B2 := strings.Join(infPart[:len(infPart)-(len(infPart)-2)][:], "")

					if (suffix4B3 == "kan") {
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (ke, peng)
							if (prefix4B4 == "peng") || (prefix4B2 == "ke") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-3]
							}
						}
					} else if (suffix4B2 == "an") {
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (di, meng, ter)
							if (prefix4B4 == "meng") || (prefix4B3 == "ter") || (prefix4B2 == "di") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-2]
							}
						}
					} else if (suffix4B1 == "i") {
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (ke, peng, ber)
							if (prefix4B4 == "peng") || (prefix4B3 == "ber") || (prefix4B2 == "ke") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-1]
							}
						}	
					} else {
						infPart = infPart[:]
					}

					// check kata baku
					if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
						newText = append(newText, strings.Join(infPart[:],""))
						continue
					}

					// step 5B- hapus Second Order Derivational Prefix
					secOrder5BPreffix3 := strings.Join(infPart[:len(infPart)-(len(infPart)-3)][:], "") // if 3 char prefix
					secOrder5BPreffix2 := strings.Join(infPart[:len(infPart)-(len(infPart)-2)][:], "") // if 3 char prefix

					if (secOrder5BPreffix3 == "ber") || (secOrder5BPreffix3 == "bel") || (secOrder5BPreffix3 == "per") || (secOrder5BPreffix3 == "pel") {
						// hapus preffix
						infPart = infPart[len(infPart)-(len(infPart)-3):]
					} else if (secOrder5BPreffix2 == "be") || (secOrder5BPreffix2 == "pe") {
						// hapus preffix
						infPart = infPart[len(infPart)-(len(infPart)-2):]
					}
				} else {
					// if not found -> step 4.a First Order Derivational Prefix

					// check kata baku
					if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
						newText = append(newText, strings.Join(infPart[:],""))
						continue
					}

					// step 4.a - hapus Second Order Derivational Prefix
					secOrder4APreffix3 := strings.Join(infPart[:len(infPart)-(len(infPart)-3)][:], "") // if 3 char prefix
					secOrder4APreffix2 := strings.Join(infPart[:len(infPart)-(len(infPart)-2)][:], "") // if 3 char prefix

					if (secOrder4APreffix3 == "ber") || (secOrder4APreffix3 == "bel") || (secOrder4APreffix3 == "per") || (secOrder4APreffix3 == "pel") {
						// hapus preffix
						infPart = infPart[len(infPart)-(len(infPart)-3):]
					} else if (secOrder4APreffix2 == "be") || (secOrder4APreffix2 == "pe") {
						// hapus preffix
						infPart = infPart[len(infPart)-(len(infPart)-2):]
					}

					// check kata baku
					if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
						newText = append(newText, strings.Join(infPart[:],""))
						continue
					}

					// step 5.a
					suffix5A3 := strings.Join(infPart[len(infPart)-3:][:], "") //5.a suffix 3
					suffix5A2 := strings.Join(infPart[len(infPart)-2:][:], "") //5.a suffix 2
					suffix5A1 := strings.Join(infPart[len(infPart)-1:][:], "") //5.a suffix 1

					prefix5A4 := strings.Join(infPart[:len(infPart)-(len(infPart)-4)][:], "")
					prefix5A3 := strings.Join(infPart[:len(infPart)-(len(infPart)-3)][:], "")
					prefix5A2 := strings.Join(infPart[:len(infPart)-(len(infPart)-2)][:], "")

					if (suffix5A3 == "kan") {
						// check kata baku
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (ke, peng)
							if (prefix5A4 == "peng") || (prefix5A2 == "ke") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-3]
							}
						}
					} else if (suffix5A2 == "an") {
						// check kata baku
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (di, meng, ter)
							if (prefix5A4 == "meng") || (prefix5A3 == "ter") || (prefix5A2 == "di") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-2]
							}
						}
					} else if (suffix5A1 == "i") {
						// check kata baku
						if (SearchBaku(strings.Join(infPart[:],"")) == "SUCCESS") {
							infPart = infPart[:]
						} else {
							// if preffix bukan anggota (ke, peng, ber)
							if (prefix5A4 == "peng") || (prefix5A3 == "ber") || (prefix5A2 == "ke") {
								// asumsi kata baku
								infPart = infPart[:]
							} else {
								// hapus suffix
								infPart = infPart[:len(infPart)-1]
							}
						}
					} else {
						infPart = infPart[:]
					}
				}
			}

			// join char
			infPartPrev := strings.Join(infPart[:],"")
			newText = append(newText, infPartPrev)
		}
	}

	// return c.String(http.StatusOK, strings.Join(newText[:]," "))
	return strings.Join(newText[:]," ")
}

// helper kata baku
func SearchBaku (textSource string) string {
	// READ KATA DASAR FILE
	var dataRoot []models.RootWord
	returnString := "FAILED"

	dataList := "storages/list/kata-dasar.json"
	fileRead, err1 := ioutil.ReadFile(dataList)
	if err1 != nil { fmt.Printf( "// error while reading file %s\n", dataList) }

	errRead := json.Unmarshal(fileRead, &dataRoot)
	if errRead != nil { fmt.Println(errRead.Error()) }

	// loop kata baku
	for _, dtr := range dataRoot {
		// if kata == kata baku { save kata }
		if (textSource == dtr.TEXT) {
			returnString = "SUCCESS"
		}
	}

	return returnString
}

// END HELPER
// ===== ***** ===== END TEXT PREPROCESSING ===== ***** ===== //