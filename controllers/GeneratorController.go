package controllers

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"encoding/json"

	"github.com/dghubble/oauth1"
	"github.com/labstack/echo/v4"
)

const CONSUMER_KEY string = "HactjSkg4VpXvZ1aErrP46OZ7"
const CONSUMER_SECRET string = "sNf3AZi7Ua4XEkot957v7PH19DH2EPMBG1KjvIoWOexhJYpJqd"
const ACCESS_TOKEN string = "1226298466894114817-SVKd17qUiRAvxWuc98Y4EwfjbHO8td"
const TOKEN_SECRET string = "yY4Ht038oITV3kSawLoX2GC9zk0WCCveOUkH645HBQHjq"

const CONSUMER_KEY_V2 string = "cdxCmJ04O7MB5KbKcQsq4gnXJ"
const CONSUMER_SECRET_V2 string = "ViuqyNyLcHIytYWUIrLDkvi6vxIiSfKKlMLiHSdyqbqijDs3K4"
const ACCESS_TOKEN_V2 string = "1226298466894114817-xWQ0CoXb33OeNyG6MDH5g2hazDCqu2"
const TOKEN_SECRET_V2 string = "TlKBhYB1mRsZ3sfPteYyGPJGCjqIt8ChlHxXvFrghdhdS"

// GET Tweet
func GetTweet (c echo.Context) error {
	var responseData interface{}
	keyword := c.Param("keyword")

	configAuth := oauth1.NewConfig(CONSUMER_KEY, CONSUMER_SECRET)
	tokenAuth := oauth1.NewToken(ACCESS_TOKEN, TOKEN_SECRET)

    httpClient := configAuth.Client(oauth1.NoContext, tokenAuth)

    path := "https://api.twitter.com/1.1/search/tweets.json?q="+ keyword +"&lang=id&count=100&tweet_mode=extended"
	resp, err := httpClient.Get(path)
	if err != nil { fmt.Println(err.Error()) }

	defer resp.Body.Close()
	
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil { fmt.Println(err.Error()) }

	err = json.Unmarshal(body, &responseData)
	if err != nil { fmt.Println(err.Error()) }
	
	return c.JSON(http.StatusOK, responseData)
}

// GET Tweet By Tweet ID
func GetTweetById (c echo.Context) error {
	var responseData interface{}
	keyword := c.Param("keyword")
	maxid := c.Param("maxid")

	configAuth := oauth1.NewConfig(CONSUMER_KEY, CONSUMER_SECRET)
	tokenAuth := oauth1.NewToken(ACCESS_TOKEN, TOKEN_SECRET)

    httpClient := configAuth.Client(oauth1.NoContext, tokenAuth)

    path := "https://api.twitter.com/1.1/search/tweets.json?q="+ keyword +"&max_id="+ maxid +"&lang=id&count=100&tweet_mode=extended"
	resp, err := httpClient.Get(path)
	if err != nil { fmt.Println(err.Error()) }

	defer resp.Body.Close()
	
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil { fmt.Println(err.Error()) }

	err = json.Unmarshal(body, &responseData)
	if err != nil { fmt.Println(err.Error()) }
	
	return c.JSON(http.StatusOK, responseData)
}

// GET Tweet By Tweet ID Limit 1
func GetTweetByIdLimit (c echo.Context) error {
	var responseData interface{}
	maxid := c.Param("maxid")

	configAuth := oauth1.NewConfig(CONSUMER_KEY_V2, CONSUMER_SECRET_V2)
	tokenAuth := oauth1.NewToken(ACCESS_TOKEN_V2, TOKEN_SECRET_V2)

    httpClient := configAuth.Client(oauth1.NoContext, tokenAuth)

    path := "https://api.twitter.com/2/tweets/"+ maxid +"?expansions=author_id&tweet.fields=created_at"
	resp, err := httpClient.Get(path)
	if err != nil { fmt.Println(err.Error()) }

	defer resp.Body.Close()
	
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil { fmt.Println(err.Error()) }

	err = json.Unmarshal(body, &responseData)
	if err != nil { fmt.Println(err.Error()) }
	
	return c.JSON(http.StatusOK, responseData)
}