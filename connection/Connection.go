package connection

import (
	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

// ConnectionMysql String
var ConnectionMysql = "root:root@tcp(127.0.0.1:8889)/golang_twitter"

// DbConnMysql Connection 
func DbConnMysql() (db *sql.DB) {
	db, err := sql.Open("mysql", ConnectionMysql)
	if err != nil { fmt.Println(err.Error()) }
	return db
}