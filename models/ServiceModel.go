package models

type ResponseJSON struct {
	SEARCH_METADATA interface{} `json: "search_metadata"`
	STATUSES []DataTweet `json: "statuses"`
}

type DataTweet struct {
	CREATED_AT string `json: "created_at"`
	ID_STR string `json: "id_str"`
	FULL_TEXT string `json: "full_text"`
	USER struct {
		SCREEN_NAME string `json: "screen_name"`
	}
}

type ReturnTweet struct {
	CREATED_AT string `json: "created_at"`
	ID_STR string `json: "id_str"`
	FULL_TEXT string `json: "full_text"`
	USER string `json: "user"`
}

type ErrorResponse struct {
	ERROR bool `json: "error"`
	MESSAGE string `json: "message"`
}

type DataDaily struct {
	RAWDAY_ID string `json: "rawday_id"`
	RAWDAY_NAME string `json: "rawday_name"`
	RAWDAY_CREATED string `json: "rawday_created"`
}

type DataWeekly struct {
	RAWWEEK_ID string `json: "rawweek_id"`
	RAWWEEK_NAME string `json: "rawweek_name"`
	RAWWEEK_CREATED string `json: "rawweek_created"`
}

type DataRepaired struct {
	RAWREPAIR_ID string `json: "rawrepair_id"`
	RAWREPAIR_NAME string `json: "rawrepair_name"`
	RAWREPAIR_CREATED string `json: "rawrepair_created"`
}

// READ OLD TWEET DATA
type OldTweet struct {
	CREATED_AT string `json : "created_at"`
	ID int `json : "id"`
	TEXT string `json : "text"`
	USER string `json : "user"`
}

// NEW SEARCH TWEET V2
type SearchUser struct {
	ID string `json : "id"`
	NAME string `json : "name"`
	USERNAME string `json : "username"`
}

type SearchTweet struct {
	DATA struct {
		ID string `json : "id"`
		TEXT string `json : "text"`
		CREATED_AT string `json : "created_at"`
	}
	INCLUDES struct {
		USERS []SearchUser `json : "users"`
	}
}

type DataRaw struct {
	CREATED_AT string `json: "created_at"`
	ID_STR string `json: "id_str"`
	FULL_TEXT string `json: "full_text"`
	USER string `json: "user"`
	LABEL string `json: "label"`
}

type TestData struct {
	TEST_ID string `json: "test_id"`
	TEST_NAME string `json: "test_name"`
	TEST_STATUS string `json: "test_status"`
	TEST_CREATED string `json: "test_created"`
}

type TokenWord struct {
	ID string `json: "id"`
	LABEL string `json: "label"`
	TEXT []string `json: "text"`
}

type StopWords struct {
	ID string `json: "id"`
	TEXT string `json: "text"`
}

type ArrayStopwords struct {
	TEXT []string `json: "text"`
}

type RootWord struct {
	TEXT string `json: "text"`
}

type TF struct {
	ID string `json: "id"`
	LABEL string `json: "label"`
	BAG []string `json: "bag"`
	TEXT string `json: "text"`
	TF []int `json: "tf"`
}

type DF struct {
	ID string `json: "id"`
	BAG []string `json: "bag"`
	DF []int `json: "tf"`
}

type IDF struct {
	ID string `json: "id"`
	BAG []string `json: "bag"`
	DF []int `json: "tf"`
	IDF []float64 `json: "idf"`
}

type TFIDF struct {
	ID string `json: "id"`
	LABEL string `json: "label"`
	BAG []string `json: "bag"`
	TEXT string `json: "text"`
	TF []int `json: "tf"`
	TFIDF []float64 `json: "tfidf"`
}

type FinalTFIDF struct {
	ID string `json: "id"`
	DF []int `json: "df"`
	IDF []float64 `json: "idf"`
	TFIDF []TFIDF `json: tfidf`
}