package main

import (
	"golang-twitter/routes"
)

func main() {
	web := routes.Init()
	web.Logger.Fatal(web.Start(":1770"))
}

