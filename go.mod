module golang-twitter

go 1.15

require (
	github.com/dghubble/oauth1 v0.7.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/tmdvs/Go-Emoji-Utils v1.1.0
)
