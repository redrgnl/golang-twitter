package routes

import (
	"net/http"

	"golang-twitter/controllers"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// Init Routes
func Init() *echo.Echo {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Pre(middleware.RemoveTrailingSlash())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))

	e.GET("/", func(c echo.Context) error {
		data := "sup hehe"
		return c.String(http.StatusOK, data)
	})

	e.GET("/tweet-grabber/:keyword", controllers.GetTweet)
	e.GET("/tweet-grabber-by-id/:keyword/:maxid", controllers.GetTweetById)
	e.GET("/tweet-grabber-by-limit/:maxid", controllers.GetTweetByIdLimit)

	e.GET("/get-tweet-data", controllers.GetTweetData)
	e.GET("/get-tweet-weekly", controllers.GetTweetWeekly)
	e.GET("/get-tweet-by-id/:id", controllers.TweetById)

	e.GET("/merge-data/:type", controllers.MergeData)
	e.GET("/repair-files", controllers.RepairData)

	e.GET("/make-data-file", controllers.MakeData) // SAVE DATA TESTING FILE
	e.GET("/test-data-by-id/:id", controllers.GetTest)
	e.GET("/text-preprocessing/:type/:id", controllers.TextPrepo)
	e.GET("/tf-idf-count/:type/:id", controllers.TermFreqInvDocFreq)
	
	e.POST("/add-stopwords", controllers.AddStopwords)
	e.POST("/add-stopwords-array", controllers.AddStopwordsArray)

	return e
}
